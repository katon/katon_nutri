;(function($, window, document, undefined) {

    $(function() {

        $("select").selectBoxIt({
            autoWidth: false
        });

        $(".owd").owlCarousel({
            mouseDrag: false,
            singleItem:true,
            paginationSpeed : 600,
            autoPlay: true
        });

        $("#telefone").mask("(99) 9999-9999");

        $("#desconto").click(function(e) {
            e.preventDefault();
            var $url = $(this).attr("href");

            $("html, body").animate({
                scrollTop: $($url).offset().top - 50
            }, 600);
        })

        $("form").submit(function(e) {
            e.preventDefault();
            var $form = $(this),
                $btn  = $("button", $form);

            $.ajax({
                url: $form.attr("action"),
                data: $form.serialize(),
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $btn.attr("disabled", "disabled").find("span, img").toggle();
                },
                success: function(data) {
                    $form.find(".red").removeClass("red");

                    if(data.erro !== undefined) {

                        $(data.erro).each(function(i, data) {
                            $("#"+ data).addClass("red");
                        });

                    } else if(data.success !== undefined) {

                        $btn.addClass("green").find("span").html("Enviado com sucesso");
                        $btn.find("span, img").toggle();

                        return;
                    }

                    $btn.removeAttr("disabled").removeClass("green").find("span, img").toggle();
                },
                error: function() {
                    alert('Erro interno. Por favor, tente novamente mais tarde.')
                }
            })
        });

    });
})(jQuery, window, document);
