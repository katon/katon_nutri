<?php

// o ajax aguarda um resultado em json
// se o objeto "erro" existir, ele trata o array dentro como erro
// se o objeto "success" existir, ele retorna sucesso

$json = array(
    //'erro' => array('nome', 'telefone', 'email')
    //'success' => true
);

$nome     = trim($_POST['nome']);
$telefone = trim($_POST['telefone']);
$email    = trim($_POST['email']);
$chegou   = trim($_POST['chegou']);

if( strlen($nome) < 8 )
    $json['erro'][] = 'nome';

if( strlen($telefone) < 8 )
    $json['erro'][] = 'telefone';

if( !filter_var($email, FILTER_VALIDATE_EMAIL) )
    $json['erro'][] = 'email';

if( !isset($json['erro']) ) {

    require('config.php');
    require('phpmailer/class.phpmailer.php');

    $mail = new PHPMailer();

    try {

        $mail->CharSet    = 'UTF-8';
        $mail->IsSMTP();                       // telling the class to use SMTP
        $mail->SMTPAuth   = true;              // enable SMTP authentication
        $mail->SMTPSecure = "ssl";             // sets the prefix to the servier
        $mail->Host       = $config['host'];   // sets GMAIL as the SMTP server
        $mail->Port       = $config['port'];   // set the SMTP port for the GMAIL server
        $mail->Username   = $config['user'];   // GMAIL username
        $mail->Password   = $config['pass'];   // GMAIL password

        $mail->SetFrom($config['from'][0], $config['from'][1]);
        $mail->AddReplyTo($config['replyto'][0], $config['replyto'][1]);
        $mail->AddAddress($config['to'][0], $config['to'][1]);
        $mail->Subject = $config['subject'];

        $mail->MsgHTML('
            <p><strong>Nome:</strong> '.$nome.'</p>
            <p><strong>Email:</strong> '.$email.'</p>
            <p><strong>Telefone:</strong> '.$telefone.'</p>
            <p><strong>Como chegou:</strong> '.$chegou.'</p>
        ');

        $mail->Send();

        $json['success'] = true;

    } catch (phpmailerException $e) {
        die($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        die($e->getMessage()); //Boring error messages from anything else!
    }
}

echo json_encode($json);