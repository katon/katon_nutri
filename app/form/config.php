<?php

date_default_timezone_set('America/Sao_Paulo');

$config = array(
    'user'    => 'contato@katon.com.br', // login
    'pass'    => 'C0nt4t0K4t0n',         // senha
    'host'    => 'smtp.gmail.com',
    'port'    => 465,
    'to'      => array('diego.roriz@katon.com.br', 'Diego Roriz'), // para
    'from'    => array('contato@katon.com.br', 'Nutri'), // de
    'replyto' => array('contato@katon.com.br', 'Nutri'), // responder a
    'subject' => '[NUTRI] Nova inscrição'  // assunto do email
);